import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { GameResponseDto, GameRequestDto } from '../../dtos/gameDto';


@Injectable()
export class GameService {
  static readonly POST_GAME_URL = environment.apiUrl + '/game';

  constructor(private httpClient: HttpClient) { }

  async postGame(requestBody: GameRequestDto): Promise<GameResponseDto> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const postBody = JSON.stringify(requestBody);

    return this.httpClient.post<GameResponseDto>(GameService.POST_GAME_URL, postBody, httpOptions).toPromise();
    }
}

import { TestBed, inject, async, getTestBed } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { GameService } from './game.service';
import { GameResponseDto, GameStatus, GameRequestDto, GameStatusCase } from '../../dtos/gameDto';
import { environment } from '../../../environments/environment';


describe('GameService', () => {
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GameService]
    });

    httpMock = getTestBed().get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([GameService], (service) => {
    expect(service).toBeTruthy();
  }));

  it('postGame should return game state on successful api call',
    async(inject([GameService], (service: GameService) => {
      const requestBody: GameRequestDto = {
        board: ['X', '', 'O', '', '', '', '', '', ''],
        move: 5,
        currentToken: 'X',
        opponentToken: 'O'
      };

      const gameStatus: GameStatus = {
        case: GameStatusCase.Ongoing
      };

      const expectedGame: GameResponseDto = {
        board: ['X', '', 'O', '', 'X', '', '', '', ''],
        gameStatus: gameStatus
      };

      service.postGame(requestBody).then(
        (data) => { expect(data).toEqual(expectedGame); },
        (error) => { fail('Did not expect error to be thrown'); }
      );

      const request = httpMock.expectOne(`${environment.apiUrl}/game`);
      expect(request.request.method).toBe('POST');
      request.flush(expectedGame);
  })));
});

import { Injectable } from '@angular/core';
import { GameRequestDto, GameResponseDto } from '../../dtos/gameDto';
import { GameService } from '../game/game.service';

@Injectable()
export class MoveService {

  constructor(private gameService: GameService) { }

  generateMoveRequest(cells: string[], move: number, currentToken: string, opponentToken: string): GameRequestDto {
    if (move != null) {
      move += 1;
    }

    const request: GameRequestDto = {
      board: cells,
      move: move,
      currentToken: currentToken,
      opponentToken: opponentToken
    };

    return request;
  }

  async sendMoveRequest(request: GameRequestDto): Promise<GameResponseDto> {
    const move = await this.gameService.postGame(request);

    return move;
  }
}

import { TestBed, inject, async, tick, fakeAsync } from '@angular/core/testing';

import { MoveService } from './move.service';
import { GameRequestDto, GameResponseDto, GameStatusCase } from '../../dtos/gameDto';
import { GameService } from '../game/game.service';

class GameServiceMock {
  postGame(requestBody: GameRequestDto): Promise<GameResponseDto> {
    return Promise.resolve({} as GameResponseDto);
  }}

describe('MoveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MoveService,
        { provide: GameService, useClass: GameServiceMock }]
    });
  });

  it('should be created', inject([MoveService], (service: MoveService) => {
    expect(service).toBeTruthy();
  }));

  it('generateMoveRequest returns a correctly formatted GameRequestDto when not move is not null',
   inject([MoveService], (service: MoveService) => {
    const playerSymbol = 'x';
    const opponentSymbol = 'o';
    const move = 9;
    const cells = [playerSymbol, playerSymbol, playerSymbol, opponentSymbol,
      opponentSymbol, '6', '7', '8', '9'];
    const expectedRequest: GameRequestDto = {
      board: cells,
      move: move,
      currentToken: playerSymbol,
      opponentToken: opponentSymbol
    };

    const request = service.generateMoveRequest(cells, move - 1, playerSymbol, opponentSymbol);

    expect(request).toEqual(expectedRequest);
  }));

  it('generateMoveRequest returns a correctly formatted GameRequestDto when move is null',
   inject([MoveService], (service: MoveService) => {
    const playerSymbol = 'x';
    const opponentSymbol = 'o';
    const move = null;
    const cells = [playerSymbol, playerSymbol, playerSymbol, opponentSymbol,
      opponentSymbol, '6', '7', '8', '9'];
    const expectedRequest: GameRequestDto = {
      board: cells,
      move: move,
      currentToken: playerSymbol,
      opponentToken: opponentSymbol
    };

    const request = service.generateMoveRequest(cells, move, playerSymbol, opponentSymbol);

    expect(request).toEqual(expectedRequest);
  }));

  it('sendMoveRequest returns GameResponseDto promise from service and updates cells',
  fakeAsync(inject([MoveService], (service: MoveService) => {
    const playerSymbol = 'x';
    const opponentSymbol = 'o';
    const initialBoardState = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const expectedBoardState = [playerSymbol, '2', '3', '4', '5', '6', '7', '8', '9'];
    const request: GameRequestDto = {
      board: initialBoardState,
      move: 1,
      currentToken: playerSymbol,
      opponentToken: opponentSymbol
    };
    const response: GameResponseDto = {
      board: expectedBoardState,
      gameStatus: {
        case: GameStatusCase.Ongoing
      }
    };
    const serviceSpy = spyOn<any>(TestBed.get(GameService), 'postGame').and.returnValue(Promise.resolve(response));

    const moveRequest = service.sendMoveRequest(request);

    tick();

    expect(serviceSpy).toHaveBeenCalledWith(request);
    expect(moveRequest).toEqual(Promise.resolve(response));
  })));
});

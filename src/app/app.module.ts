import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CellComponent } from './components/cell/cell.component';
import { GameContainerComponent } from './components/game-container/game-container.component';
import { BoardComponent } from './components/board/board.component';
import { GameService } from './services/game/game.service';
import { OverlayComponent } from './components/overlay/overlay.component';
import { HeaderComponent } from './components/header/header.component';
import { NowPlayingComponent } from './components/now-playing/now-playing.component';
import { MoveService } from './services/move/move.service';
import { MessageBubbleComponent } from './components/message-bubble/message-bubble.component';

@NgModule({
  declarations: [
    AppComponent,
    CellComponent,
    GameContainerComponent,
    BoardComponent,
    OverlayComponent,
    HeaderComponent,
    NowPlayingComponent,
    MessageBubbleComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    GameService,
    MoveService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';

import { BoardComponent } from './board.component';
import { By } from '@angular/platform-browser';
import { CellComponent } from '../cell/cell.component';
import { GameRequestDto, GameResponseDto, GameStatusCase } from '../../dtos/gameDto';
import { OverlayComponent } from '../overlay/overlay.component';
import { PlayerDto, PlayerType } from '../../dtos/playerDto';
import { MoveService } from '../../services/move/move.service';

class MoveServiceMock {
  generateMoveRequest(cells: string[], move: number, currentToken: string, opponentToken: string): GameRequestDto {
    return {} as GameRequestDto;
  }

  async sendMoveRequest(request: GameRequestDto): Promise<GameResponseDto> {
    return Promise.resolve({} as GameResponseDto);
  }}

describe('BoardComponent', () => {
  let component: BoardComponent;
  let fixture: ComponentFixture<BoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardComponent, CellComponent, OverlayComponent ],
      providers: [
        { provide: MoveService, useClass: MoveServiceMock }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardComponent);
    component = fixture.componentInstance;
    const player: PlayerDto = {
      symbol: 'X',
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: 'O',
      type: PlayerType.Bot
    };
    component.player = player;
    component.opponent = opponent;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('creates the correct amount of cells for the board', () => {
    const boardCells = ['X', '2', 'O'];
    component.cells = boardCells;

    fixture.detectChanges();
    const cells = fixture.debugElement.queryAll(By.css('app-cell'));

    expect(cells.length).toBe(3);
  });

  it('makeMove makes a call to the GameService with the correct board information and initiates computer turn', fakeAsync(() => {
    const player: PlayerDto = {
      symbol: 'X',
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: 'O',
      type: PlayerType.Bot
    };

    const boardState: string[] = [player.symbol, '', opponent.symbol, '', '', '', '', '', ''];
    const expectedBoardState: string[] = [player.symbol, '', opponent.symbol, '', player.symbol, '', '', '', ''];

    const responseData: GameResponseDto = {
      board: expectedBoardState,
      gameStatus: {
        case: GameStatusCase.Ongoing
      }
    };

    const requestData: GameRequestDto = {
      board: boardState,
      move: 4,
      currentToken: player.symbol,
      opponentToken: opponent.symbol
    };

    const secondRequestData: GameRequestDto = {
      board: expectedBoardState,
      move: null,
      currentToken: opponent.symbol,
      opponentToken: player.symbol
    };

    const sendMoveSpy = spyOn<any>(TestBed.get(MoveService), 'sendMoveRequest').and.returnValue(Promise.resolve(responseData));
    const generateMoveSpy = spyOn<any>(TestBed.get(MoveService), 'generateMoveRequest')
      .and.returnValues(requestData, secondRequestData);

    component.cells = boardState;
    component.player = player;
    component.opponent = opponent;
    component.makeMove(4, player, opponent);

    tick();

    expect(sendMoveSpy).toHaveBeenCalledWith(requestData);
    expect(sendMoveSpy).toHaveBeenCalledWith(secondRequestData);
    expect(sendMoveSpy).toHaveBeenCalledTimes(3);
    expect(generateMoveSpy).toHaveBeenCalledWith(boardState, requestData.move, player.symbol, opponent.symbol);
    expect(generateMoveSpy).toHaveBeenCalledWith(expectedBoardState, null, opponent.symbol, player.symbol);
    expect(component.cells).toBe(expectedBoardState);
  }));

  it('makeMove displays label when game is a tie', fakeAsync(() => {
    const player: PlayerDto = {
      symbol: 'X',
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: 'O',
      type: PlayerType.Bot
    };
    const boardState: string[] = [player.symbol, opponent.symbol, '3', player.symbol, player.symbol,
     opponent.symbol, opponent.symbol, player.symbol, opponent.symbol];
    const expectedBoardState: string[] = [player.symbol, opponent.symbol, player.symbol, player.symbol, player.symbol,
    opponent.symbol, opponent.symbol, player.symbol, opponent.symbol];

    const responseData: GameResponseDto = {
      board: expectedBoardState,
      gameStatus: {
        case: GameStatusCase.Tie
      }
    };

    const requestData: GameRequestDto = {
      board: boardState,
      move: 3,
      currentToken: player.symbol,
      opponentToken: opponent.symbol
    };

    const sendMoveSpy = spyOn<any>(TestBed.get(MoveService), 'sendMoveRequest').and.returnValue(Promise.resolve(responseData));
    spyOn<any>(TestBed.get(MoveService), 'generateMoveRequest')
      .and.returnValue(requestData);

    component.cells = boardState;
    component.player = player;
    component.opponent = opponent;
    component.makeMove(2, player, opponent);

    tick();

    fixture.detectChanges();

    const gameStatusLabel = fixture.debugElement.query(By.css('[data-id=gameStatusLabel]'));

    expect(sendMoveSpy).toHaveBeenCalledWith(requestData);
    expect(component.cells).toBe(expectedBoardState);
    expect(gameStatusLabel).not.toBeNull();
    expect(gameStatusLabel.nativeElement.innerHTML).toContain(BoardComponent.TIED_GAME_MESSAGE);
    expect(component.gameStatusMessage).toBe(BoardComponent.TIED_GAME_MESSAGE);
  }));

  it('makeMove displays label when game won with correct message', fakeAsync(() => {
    const player: PlayerDto = {
      symbol: 'X',
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: 'O',
      type: PlayerType.Bot
    };
    const boardState: string[] = [player.symbol, player.symbol, '3', opponent.symbol, opponent.symbol, '6', '7', '8', '9'];
    const expectedBoardState: string[] = [player.symbol, player.symbol, player.symbol, opponent.symbol,
       opponent.symbol, '6', '7', '8', '9'];

    const responseData: GameResponseDto = {
      board: expectedBoardState,
      gameStatus: {
        case: GameStatusCase.Win
      }
    };

    const requestData: GameRequestDto = {
      board: boardState,
      move: 3,
      currentToken: player.symbol,
      opponentToken: opponent.symbol
    };

    const sendMoveSpy = spyOn<any>(TestBed.get(MoveService), 'sendMoveRequest').and.returnValue(Promise.resolve(responseData));
    spyOn<any>(TestBed.get(MoveService), 'generateMoveRequest')
      .and.returnValue(requestData);

    component.cells = boardState;
    component.player = player;
    component.opponent = opponent;
    component.makeMove(2, player, opponent);

    tick();

    fixture.detectChanges();

    const gameStatusLabel = fixture.debugElement.query(By.css('[data-id=gameStatusLabel]'));

    expect(sendMoveSpy).toHaveBeenCalledWith(requestData);
    expect(component.cells).toBe(expectedBoardState);
    expect(gameStatusLabel).not.toBeNull();
    expect(gameStatusLabel.nativeElement.innerHTML).toContain(component.createPlayerWinMessage(player.symbol));
    expect(component.gameStatusMessage).toBe(component.createPlayerWinMessage(player.symbol));
  }));

  it('makeMove displays error label when error thrown from game service', fakeAsync(() => {
    const player: PlayerDto = {
      symbol: 'X',
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: 'O',
      type: PlayerType.Bot
    };
    const boardState: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const response = new HttpResponse();

    const sendMoveSpy = spyOn<any>(TestBed.get(MoveService), 'sendMoveRequest').and.returnValue(Promise.reject('because'));
    spyOn<any>(TestBed.get(MoveService), 'generateMoveRequest');

    const errorLabel = () => fixture.debugElement.query(By.css('[data-id=errorLabel]'));
    expect(errorLabel()).toBeNull('Error display div should not be visible yet');

    component.cells = [];
    component.player = player;
    component.opponent = opponent;
    component.makeMove(2, player, opponent);

    tick();

    fixture.detectChanges();

    expect(errorLabel()).not.toBeNull();
    expect(errorLabel().nativeElement.innerHTML).toContain(component.errorMessage);
  }));

  it('makeMove if a player has already made that move it exits function', () => {
    const player: PlayerDto = {
      symbol: 'X',
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: 'O',
      type: PlayerType.Bot
    };
    const boardState: string[] = [player.symbol, player.symbol, '3', opponent.symbol, opponent.symbol, '6', '7', '8', '9'];

    const sendMoveSpy = spyOn<any>(TestBed.get(MoveService), 'sendMoveRequest');
    const generateMoveSpy = spyOn<any>(TestBed.get(MoveService), 'generateMoveRequest');

    component.cells = boardState;
    component.player = player;
    component.opponent = opponent;

    component.makeMove(0, null, null);

    expect(sendMoveSpy).not.toHaveBeenCalled();
    expect(generateMoveSpy).not.toHaveBeenCalled();
  });

  it('setPlayerTieMessage sets the gameStatusMessage to the tie game message', () => {
    expect(component.gameStatusMessage).not.toBe(BoardComponent.TIED_GAME_MESSAGE);

    component.setPlayerTieMessage();

    expect(component.gameStatusMessage).toBe(BoardComponent.TIED_GAME_MESSAGE);
  });

  it('createPlayerWinMessage correctly creates the player message', () => {
    const expectedMessage = 'player X wins!';
    const playerToken = 'X';

    expect(component.createPlayerWinMessage(playerToken)).toBe(expectedMessage);
  });

  it('createOpponentWinMessage correctly creates the opponent win message', () => {
    const expectedBotMessage = 'tic-tac-tobot wins!';
    const expectedHumanMessage = 'player o wins!';
    const botPlayer: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Bot
    };
    const humanPlayer: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Human
    };

    component.opponent = botPlayer;
    const botMessage = component.createOpponentWinMessage();
    expect(botMessage).toBe(expectedBotMessage);

    component.opponent = humanPlayer;
    const humanMessage = component.createOpponentWinMessage();
    expect(humanMessage).toBe(expectedHumanMessage);
  });

  it('setPlayerWinMessage sets the gameStatusMessage to the correct value', () => {
    const playerSymbol = 'x';
    const opponentSymbol = 'o';
    const player: PlayerDto = {
      symbol: playerSymbol,
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: opponentSymbol,
      type: PlayerType.Human
    };
    const expectedPlayerMessage = 'player ' + playerSymbol + ' wins!';
    const expectedOpponentMessage = 'player ' + opponentSymbol + ' wins!';

    component.player = player;
    component.opponent = opponent;

    component.setPlayerWinMessage(playerSymbol);
    expect(component.gameStatusMessage).toBe(expectedPlayerMessage);

    component.setPlayerWinMessage(opponentSymbol);
    expect(component.gameStatusMessage).toBe(expectedOpponentMessage);
  });

  it('restartGame resets variables properly', () => {
    const expectedCells = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    spyOn(component, 'resetCurrentPlayer');
    spyOn(component.emitMove, 'emit');
    const player: PlayerDto = {
      symbol: 'x',
      type: PlayerType.Human
    };

    component.showError = true;
    component.showGameStatus = true;
    component.preventNextMove = true;
    component.currentPlayer = player;
    component.cells = ['1', 'X', 'O', 'X', '5', '6', '7', '8', '9'];

    component.restartGame();

    expect(component.cells).toEqual(expectedCells);
    expect(component.showError).toBeFalsy();
    expect(component.showGameStatus).toBeFalsy();
    expect(component.preventNextMove).toBeFalsy();
    expect(component.resetCurrentPlayer).toHaveBeenCalled();
    expect(component.emitMove.emit).toHaveBeenCalledWith(player);
  });

  it('showGameStatusOverlay shows the overlay that displays the game status', () => {
    const playerSymbol = 'x';
    const expectedStatusMessage = component.createPlayerWinMessage(playerSymbol);
    const expectedRestartMessage = 'click anywhere to restart';
    const message = () => fixture.nativeElement.querySelector('.status');
    const restartLabel = () => fixture.nativeElement.querySelector('.restart');

    expect(message()).toBeNull();
    expect(restartLabel()).toBeNull();

    component.gameStatusMessage = expectedStatusMessage;
    component.showGameStatusOverlay(true);
    fixture.detectChanges();

    const overlay = fixture.nativeElement.querySelector('[data-id=gameStatusOverlay]');
    expect(overlay).not.toBeNull();
    expect(message().innerHTML).toBe(expectedStatusMessage);
    expect(restartLabel().innerHTML).toBe(expectedRestartMessage);
    expect(component.showGameStatus).toBeTruthy();
    expect(component.showOverlay).toBeTruthy();
  });

  it('hideOverlay hides the overlay if it present and resets cells', () => {
    spyOn(component, 'restartGame');
    const expectedCells = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];

    component.gameStatusMessage = 'valid message';
    component.showGameStatusOverlay(true);
    fixture.detectChanges();

    const overlay = () => fixture.nativeElement.querySelector('[data-id=gameStatusOverlay]');
    expect(overlay()).not.toBeNull();

    component.hideOverlay();
    fixture.detectChanges();

    expect(overlay()).toBeNull();
    expect(component.showOverlay).toBeFalsy();
    expect(component.showGameStatus).toBeFalsy();
    expect(component.showError).toBeFalsy();
    expect(component.restartGame).toHaveBeenCalled();
  });

  it('changeCurrentPlayer changes the currentPlayer to the opposite player and emits event', () => {
    const player: PlayerDto = {
      symbol: 'x',
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Bot
    };
    spyOn(component.emitMove, 'emit');

    component.player = player;
    component.opponent = opponent;
    component.currentPlayer = player;
    component.changeCurrentPlayer();

    expect(component.currentPlayer).toBe(opponent);
    expect(component.emitMove.emit).toHaveBeenCalledWith(opponent);
  });

  it('getOpponentPlayer returns the opposite player of the current player', () => {
    const player: PlayerDto = {
      symbol: 'x',
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Bot
    };

    component.player = player;
    component.opponent = opponent;
    component.currentPlayer = player;
    expect(component.getOpponentPlayer()).toBe(opponent);

    component.currentPlayer = opponent;
    expect(component.getOpponentPlayer()).toBe(player);
  });

  it('resetCurrentPlayer resets the currentPlayer to player and allows moves', () => {
    const player: PlayerDto = {
      symbol: 'x',
      type: PlayerType.Human
    };
    const opponent: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Bot
    };

    component.player = player;
    component.opponent = opponent;
    component.currentPlayer = opponent;
    component.preventNextMove = true;
    component.resetCurrentPlayer();

    expect(component.currentPlayer).toBe(player);
    expect(component.preventNextMove).toBeFalsy();
  });
});

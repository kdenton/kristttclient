import { Component, OnInit, Input, SimpleChanges, HostListener, OnChanges, Output, EventEmitter } from '@angular/core';

import { GameRequestDto, GameResponseDto, GameStatusCase } from '../../dtos/gameDto';
import { PlayerDto, PlayerType } from '../../dtos/playerDto';
import { MoveService } from '../../services/move/move.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit, OnChanges {
  static readonly TIED_GAME_MESSAGE = 'the game is tied!';
  static readonly POST_GAME_ERROR = 'oops! something went wrong...';

  @Output() emitMove: EventEmitter<PlayerDto> = new EventEmitter<PlayerDto>();
  @Input() cells: string[];
  @Input() player: PlayerDto;
  @Input() opponent: PlayerDto;

  showGameStatus: boolean;
  showError: boolean;
  showOverlay: boolean;
  gameStatusMessage: string;
  errorMessage: string;
  preventNextMove = false;
  currentPlayer: PlayerDto;

  constructor(private moveService: MoveService) {
   }

  ngOnInit() {
    this.currentPlayer = this.player;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.cells.previousValue !== changes.cells.currentValue) {
      this.resetCurrentPlayer();
    }
  }

  setPlayerTieMessage() {
    this.gameStatusMessage = BoardComponent.TIED_GAME_MESSAGE;
  }

  createPlayerWinMessage(token: string) {
    return 'player ' + token + ' wins!';
  }

  createOpponentWinMessage() {
    if (this.opponent.type === PlayerType.Bot) {
      return 'tic-tac-tobot wins!';
    }

    return this.createPlayerWinMessage(this.opponent.symbol);
  }

  setPlayerWinMessage(token: string) {
    if (token === this.player.symbol) {
      this.gameStatusMessage = this.createPlayerWinMessage(token);
    } else {
      this.gameStatusMessage = this.createOpponentWinMessage();
    }
  }

  makeMove(index: number, currentPlayer: PlayerDto, opponentPlayer: PlayerDto) {
    if (!this.validMove(index)) {
      return;
    }

    this.preventNextMove = true;

    const request = this.moveService.generateMoveRequest(this.cells, index, currentPlayer.symbol, opponentPlayer.symbol);

    this.moveService.sendMoveRequest(request).then(
    (response) => {
      this.cells = response.board;
      this.changeCurrentPlayer();

      switch (response.gameStatus.case) {
        case GameStatusCase.Ongoing: {
          if (currentPlayer === this.player && this.opponent.type === PlayerType.Bot) {
            this.makeMove(null, this.currentPlayer, this.getOpponentPlayer());
          } else {
            this.preventNextMove = false;
          }
          break;
        }
        case GameStatusCase.Tie: {
          this.setPlayerTieMessage();
          this.showGameStatusOverlay(true);
          break;
        }
        case GameStatusCase.Win: {
          this.setPlayerWinMessage(request.currentToken);
          this.showGameStatusOverlay(true);
          break;
        }
      }
    },
    (error) => {
      this.errorMessage = BoardComponent.POST_GAME_ERROR;
      this.showGameStatusOverlay(false);
    });
  }

  validMove(index: number) {
    if (this.cells[index] === this.player.symbol || this.cells[index] === this.opponent.symbol) {
      return false;
    }

    if (this.preventNextMove && this.opponent.type === PlayerType.Bot && index !== null) {
      return false;
    }

    return true;
  }

  restartGame() {
    this.cells = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    this.showGameStatus = false;
    this.showError = false;
    this.resetCurrentPlayer();
    this.emitMove.emit(this.currentPlayer);
    this.preventNextMove = false;
  }

  showGameStatusOverlay(showStatus: boolean) {
    this.showGameStatus = showStatus;
    this.showError = !showStatus;
    this.showOverlay = true;
  }

  hideOverlay() {
    this.restartGame();
    this.showOverlay = false;
    this.showGameStatus = false;
    this.showError = false;
  }

  changeCurrentPlayer() {
    this.currentPlayer = this.getOpponentPlayer();
    this.emitMove.emit(this.currentPlayer);
  }

  getOpponentPlayer() {
    return this.currentPlayer === this.player ? this.opponent : this.player;
  }

  resetCurrentPlayer() {
    this.currentPlayer = this.player;
    this.preventNextMove = false;
  }
}

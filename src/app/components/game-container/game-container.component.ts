import { Component, OnInit } from '@angular/core';

import { PlayerType, PlayerDto } from '../../dtos/playerDto';

@Component({
  selector: 'app-game-container',
  templateUrl: './game-container.component.html',
  styleUrls: ['./game-container.component.css']
})
export class GameContainerComponent implements OnInit {
  cells: string[];
  player: PlayerDto;
  opponent: PlayerDto;
  currentPlayer: PlayerDto;

  constructor() { }

  ngOnInit() {
    this.restartGame();
    const newPlayer: PlayerDto = {symbol: 'x', type: PlayerType.Human};
    const newOpponent: PlayerDto = {symbol: 'o', type: PlayerType.Bot};

    this.player = newPlayer;
    this.opponent = newOpponent;
    this.currentPlayer = this.player;
  }

  restartGame() {
    this.cells = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
  }

  changeOpponent(event) {
    this.opponent.type = event;
    this.restartGame();
  }

  switchCurrentPlayer(event) {
    this.currentPlayer = event;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameContainerComponent } from './game-container.component';
import { By } from '@angular/platform-browser';
import { BoardComponent } from '../board/board.component';
import { CellComponent } from '../cell/cell.component';
import { GameService } from '../../services/game/game.service';
import { OverlayComponent } from '../overlay/overlay.component';
import { HeaderComponent } from '../header/header.component';
import { NowPlayingComponent } from '../now-playing/now-playing.component';
import { PlayerType, PlayerDto } from '../../dtos/playerDto';
import { MoveService } from '../../services/move/move.service';
import { MessageBubbleComponent } from '../message-bubble/message-bubble.component';

describe('GameContainerComponent', () => {
  let component: GameContainerComponent;
  let fixture: ComponentFixture<GameContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameContainerComponent, BoardComponent, CellComponent, OverlayComponent,
         HeaderComponent, NowPlayingComponent, MessageBubbleComponent ],
      providers: [ { provide: GameService }, { provide: MoveService } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain the board', () => {
    const board = fixture.debugElement.query(By.css('app-board'));

    expect(board).not.toBeNull();
  });

  it('restartGame sets the board to default values', () => {
    const expectedValue = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const currentValue = ['x', 'o', 'x', 'o', '5', '6', '7', '8', '9'];
    component.cells = currentValue;

    expect(component.cells).toEqual(currentValue);

    component.restartGame();

    expect(component.cells).toEqual(expectedValue);
  });

  it('changeOpponent sets the opponent type to the proper value and restarts the game', () => {
    const expectedType = PlayerType.Human;
    spyOn(component, 'restartGame');

    expect(component.opponent.type).toBe(PlayerType.Bot);

    component.changeOpponent(expectedType);

    expect(component.opponent.type).toBe(expectedType);
    expect(component.restartGame).toHaveBeenCalled();
  });

  it('switchCurrentPlayer sets the currentPlayer to the proper value', () => {
    const expectedPlayer: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Bot
    };

    const initialPlayer: PlayerDto = {
      symbol: 'x',
      type: PlayerType.Human
    };

    component.ngOnInit();
    expect(component.currentPlayer).toEqual(initialPlayer);

    component.switchCurrentPlayer(expectedPlayer);

    expect(component.currentPlayer).toEqual(expectedPlayer);
  });
});

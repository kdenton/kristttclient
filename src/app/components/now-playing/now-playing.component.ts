import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';

import { PlayerType, PlayerDto } from '../../dtos/playerDto';

@Component({
  selector: 'app-now-playing',
  templateUrl: './now-playing.component.html',
  styleUrls: ['./now-playing.component.css']
})

export class NowPlayingComponent implements OnInit, OnChanges {
  @Output() opponentChanged: EventEmitter<PlayerType> = new EventEmitter<PlayerType>();
  @Input() opponent: PlayerDto;
  @Input() player: PlayerDto;
  @Input() currentPlayer: PlayerDto;

  playerStyle: string;
  opponentStyle: string;
  botType = PlayerType.Bot;
  opponentActiveStyle: string;
  playerActiveStyle: string;
  ticTacTobotMessage = false;
  playerMessage = false;
  firstTimePlay = true;

  constructor() { }

  ngOnInit() {
    this.playerStyle = this.getRandomHeadStyle();
    this.opponentStyle = this.getRandomHeadStyle();
    this.currentPlayer = this.player;
    this.setOpponentStyle();
    this.setFirstTimePlayStyle();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setCurrentPlayerActiveStyle();
  }

  getRandomHeadStyle() {
    const headNum = Math.floor(Math.random() * 7) + 1;

    return 'head-' + headNum;
  }

  setCurrentPlayerActiveStyle() {
    if (this.opponent.type !== PlayerType.Bot) {
      this.currentPlayer === this.player ? this.playerActiveStyle = 'active' : this.playerActiveStyle = '';
      this.currentPlayer === this.opponent ? this.opponentActiveStyle = 'active' : this.opponentActiveStyle = '';
    } else {
      this.playerActiveStyle = '';
      this.opponentActiveStyle = '';
    }
  }

  setOpponentStyle() {
    if (this.opponent.type === PlayerType.Bot) {
      this.opponentStyle = 'tic-tac-tobot';
    } else {
      this.opponentStyle = this.getRandomHeadStyle();
    }
  }

  changeOpponent() {
    this.opponent.type = this.opponent.type === PlayerType.Bot ? PlayerType.Human : PlayerType.Bot;
    this.opponentChanged.emit(this.opponent.type);
    this.currentPlayer = this.player;
    this.setCurrentPlayerActiveStyle();
    this.setOpponentStyle();
    this.setFirstTimePlayStyle();
  }

  setFirstTimePlayStyle() {
    if (this.firstTimePlay && this.ticTacTobotMessage === false && this.playerMessage === false) {
      this.ticTacTobotMessage = true;
    } else if (this.firstTimePlay && this.ticTacTobotMessage === true && this.playerMessage === false) {
      this.ticTacTobotMessage = false;
      this.playerMessage = true;
    } else {
      this.firstTimePlay = false;
    }
  }
}

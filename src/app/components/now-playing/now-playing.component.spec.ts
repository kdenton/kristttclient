import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NowPlayingComponent } from './now-playing.component';
import { PlayerDto, PlayerType } from '../../dtos/playerDto';
import { MessageBubbleComponent } from '../message-bubble/message-bubble.component';

describe('NowPlayingComponent', () => {
  let component: NowPlayingComponent;
  let fixture: ComponentFixture<NowPlayingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NowPlayingComponent, MessageBubbleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NowPlayingComponent);
    component = fixture.componentInstance;
    const player: PlayerDto = {
      symbol: 'x',
      type: PlayerType.Human
    };

    const opponent: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Bot
    };

    component.player = player;
    component.opponent = opponent;

    fixture.detectChanges();
  });

  it('should create', () => {
    component.ngOnInit();
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('changeOpponent changes the opponent type and emits the type and applies the correct style', () => {
    const player: PlayerDto = {
      symbol: 'x',
      type: PlayerType.Human
    };
    const botOpponent: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Bot
    };
    const humanOpponent: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Human
    };

    const opponentHead = () => fixture.nativeElement.querySelector('.player.tic-tac-tobot.opponent');
    const opponentName = () => fixture.nativeElement.querySelector('[data-id=opponentName]');
    spyOn(component.opponentChanged, 'emit');
    spyOn(component, 'getRandomHeadStyle');
    spyOn(component, 'setCurrentPlayerActiveStyle');

    expect(opponentHead()).not.toBeNull();
    expect(opponentName().innerHTML).toBe('tic-tac-tobot');
    expect(component.opponent.type).toBe(PlayerType.Bot);

    component.player = player;
    component.opponent = botOpponent;
    component.currentPlayer = botOpponent;
    component.changeOpponent();
    component.opponentStyle = 'head-5';
    fixture.detectChanges();

    const newOpponentHead = fixture.nativeElement.querySelector('.player.head-5.opponent');

    expect(component.opponent.type).toBe(PlayerType.Human);
    expect(opponentHead()).toBeNull();
    expect(newOpponentHead).not.toBeNull();
    expect(opponentName().innerHTML).toBe('opponent');
    expect(component.opponent.type).toBe(PlayerType.Human);
    expect(component.opponentChanged.emit).toHaveBeenCalledWith(PlayerType.Human);
    expect(component.getRandomHeadStyle).toHaveBeenCalled();
    expect(component.currentPlayer).toBe(player);
    expect(component.setCurrentPlayerActiveStyle).toHaveBeenCalled();
  });

  it('getStyle returns the string "head-" and a number between 1 and 7', () => {
    const style = component.getRandomHeadStyle();

    expect(style.length).toBe(6);
    expect(parseInt(style.substr(style.length - 1), 10)).toBeLessThanOrEqual(7);
    expect(parseInt(style.substr(style.length - 1), 10)).toBeGreaterThanOrEqual(1);
    expect(style.substr(0, 5)).toBe('head-');
  });

  it('setCurrentPlayerActiveStyle sets the proper active class string depending on currentPlayer and opponent type', () => {
    const player: PlayerDto = {
      symbol: 'x',
      type: PlayerType.Human
    };
    const botOpponent: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Bot
    };
    const humanOpponent: PlayerDto = {
      symbol: 'o',
      type: PlayerType.Human
    };
    const activeStyle = 'active';

    component.player = player;
    component.opponent = botOpponent;
    component.currentPlayer = player;
    component.setCurrentPlayerActiveStyle();

    expect(component.opponentActiveStyle).toBe('');
    expect(component.playerActiveStyle).toBe('');

    component.opponent = humanOpponent;
    component.setCurrentPlayerActiveStyle();

    expect(component.opponentActiveStyle).toBe('');
    expect(component.playerActiveStyle).toBe(activeStyle);

    component.currentPlayer = humanOpponent;
    component.setCurrentPlayerActiveStyle();

    expect(component.opponentActiveStyle).toBe(activeStyle);
    expect(component.playerActiveStyle).toBe('');
  });

  it('setFirstTimePlayStyle shows messages at appropriate times', () => {
    const firstTimePlay = () => fixture.nativeElement.querySelector('[data-id=message-bubble]');
    const ticTacTobotHeadStyle = () => fixture.nativeElement.querySelector('.tic-tac-tobot.header-head');
    const playerHeadStyle = () => fixture.nativeElement.querySelector('.head-5.header-head');
    const headerText = fixture.nativeElement.querySelector('[data-id=header-text]').innerHTML;
    const expectedHeaderText = 'did you know?';
    const message = () => fixture.nativeElement.querySelector('[data-id=message]').innerHTML;
    const expectedTTTMessageOne = 'you can click on me to play';
    const expectedTTTMessageTwo = 'against a friend';
    const expectedPlayerMessageOne = 'and you can click on';
    const expectedPlayerMessageTwo = 'play against tic-tac-tobot';

    expect(component.firstTimePlay).toBeTruthy();
    expect(component.ticTacTobotMessage).toBeTruthy();
    expect(component.playerMessage).toBeFalsy();
    expect(firstTimePlay()).not.toBeNull();
    expect(ticTacTobotHeadStyle()).not.toBeNull();
    expect(playerHeadStyle()).toBeNull();
    expect(headerText).toBe(expectedHeaderText);
    expect(message()).toContain(expectedTTTMessageOne);
    expect(message()).toContain(expectedTTTMessageTwo);
    expect(message()).not.toContain(expectedPlayerMessageOne);
    expect(message()).not.toContain(expectedPlayerMessageTwo);

    component.opponentStyle = 'head-5';
    component.setFirstTimePlayStyle();
    fixture.detectChanges();

    expect(component.firstTimePlay).toBeTruthy();
    expect(component.ticTacTobotMessage).toBeFalsy();
    expect(component.playerMessage).toBeTruthy();
    expect(ticTacTobotHeadStyle()).toBeNull();
    expect(playerHeadStyle()).not.toBeNull();
    expect(message()).toContain(expectedPlayerMessageOne);
    expect(message()).toContain(expectedPlayerMessageTwo);
    expect(message()).not.toContain(expectedTTTMessageOne);
    expect(message()).not.toContain(expectedTTTMessageTwo);

    component.setFirstTimePlayStyle();
    fixture.detectChanges();

    expect(firstTimePlay()).toBeNull();
  });
});

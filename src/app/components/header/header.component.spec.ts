import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain name and logo', () => {
    const expectedName = 'tic-tac-toe';
    const name = fixture.nativeElement.querySelector('.name');
    const logo = fixture.nativeElement.querySelector('.logo');

    expect(name.innerHTML).toBe(expectedName);
    expect(logo).not.toBeNull();
  });
});

import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.css']
})
export class OverlayComponent {
  @Output() overlayClicked: EventEmitter<any> = new EventEmitter();

  onOverlayClicked() {
    this.overlayClicked.emit();
  }
}

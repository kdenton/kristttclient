import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})

export class CellComponent implements OnInit {
  @Input() cellValue: string;
  @Input() playerSymbol: string;
  @Input() opponentSymbol: string;

  ngOnInit() {
  }

  getStyle() {
    switch (this.cellValue) {
      case this.playerSymbol: {
        return 'player-cell';
      }
      case this.opponentSymbol: {
        return 'opponent-cell';
      }
      default: {
        return 'unused-cell';
      }
    }
  }
}

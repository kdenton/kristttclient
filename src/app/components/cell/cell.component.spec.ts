import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CellComponent } from './cell.component';
import {By} from '@angular/platform-browser';

describe('CellComponent', () => {
  let component: CellComponent;
  let fixture: ComponentFixture<CellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('renders the correct cell when X', () => {
    const playerSymbol = 'X';
    const opponentSymbol = 'O';

    component.cellValue = playerSymbol;
    component.playerSymbol = playerSymbol;
    component.opponentSymbol = opponentSymbol;

    fixture.detectChanges();

    const cell = fixture.debugElement.query(By.css('div[class="cell player-cell"]'));
    expect(cell).not.toBeNull();
  });

  it('renders the correct cell when O', () => {
    const playerSymbol = 'X';
    const opponentSymbol = 'O';

    component.cellValue = opponentSymbol;
    component.playerSymbol = playerSymbol;
    component.opponentSymbol = opponentSymbol;

    fixture.detectChanges();

    const cell = fixture.debugElement.query(By.css('div[class="cell opponent-cell"]'));
    expect(cell).not.toBeNull();
  });

  it('renders the correct cell when unused', () => {
    const playerSymbol = 'X';
    const opponentSymbol = 'O';

    component.cellValue = '5';
    component.playerSymbol = playerSymbol;
    component.opponentSymbol = opponentSymbol;

    fixture.detectChanges();

    const cell = fixture.debugElement.query(By.css('div[class="cell unused-cell"]'));
    expect(cell).not.toBeNull();
  });

  it('getStyle returns the correct style depending on the value', () => {
    const playerSymbol = 'x';
    const opponentSymbol = 'o';
    const expectedPlayerStyle = 'player-cell';
    const expectedOpponentStyle = 'opponent-cell';
    const expectedDefaultStyle = 'unused-cell';

    component.playerSymbol = playerSymbol;
    component.opponentSymbol = opponentSymbol;

    component.cellValue = playerSymbol;
    const playerStyle = component.getStyle();
    expect(playerStyle).toBe(expectedPlayerStyle);

    component.cellValue = opponentSymbol;
    const opponentStyle = component.getStyle();
    expect(opponentStyle).toBe(expectedOpponentStyle);

    component.cellValue = '1';
    const defaultStyle = component.getStyle();
    expect(defaultStyle).toBe(expectedDefaultStyle);
  });
});

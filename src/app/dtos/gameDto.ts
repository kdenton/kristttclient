export interface GameResponseDto {
    board: string[];
    gameStatus: GameStatus;
}

export interface GameStatus {
    case: GameStatusCase;
}

export interface GameRequestDto {
    board: string[];
    move: number;
    currentToken: string;
    opponentToken: string;
}

export enum GameStatusCase {
    Ongoing = 'Ongoing',
    Tie = 'Tie',
    Win = 'Win'
}

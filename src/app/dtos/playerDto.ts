export interface PlayerDto {
    symbol: string;
    type: PlayerType;
}

export enum PlayerType {
    Human = 'Human',
    Bot = 'Bot'
}
